#!/usr/bin/env perl
use warnings;
use strict;
use v5.16;

use Getopt::Long;
use Term::ANSIColor;
use FindBin qw/$RealBin/;

my %OPTS = (
  prefix => 'hg-',
  baseurl => 'bitbucket.org',
  workdir => "$RealBin/workdir",
);
GetOptions(\%OPTS, qw/help! prefix=s baseurl=s workdir=s username=s from=s to=s/);

sub note
{
  my ($key, $text, $colour) = @_;
  $colour //= 'cyan';
  say colored($key, "bold $colour") . colored($text, $colour);
}


sub help
{
  my ($err) = @_;
  note $err, '', 'red' if $err;
  say <<EOF;
Usage: $0 --username my-name --from $OPTS{prefix}my-repo <--to my-repo>
  Converts the given Mercurial repo to a Git one, and pushes to Bitbucket.
  
  --prefix $OPTS{prefix}
    Change the expected repository name prefix that indicates a Mercurial repo.
  
  --baseurl $OPTS{baseurl}
    Change url of bitbucket.org. In case you're using ssh config hosts to switch keys.
  
  --workdir $OPTS{workdir}
    Change where we download repositories to.
  
  --username <username>
    Specify what your bitbucket username is, for building paths.
  
  --from <hg-repo>
    Specify the name of the hg repo to convert.
  
  --to <git-repo>
    Specify the name of the git repo to push to.
    If not given, will be inferred based on the hg repo name.
EOF
  exit 1;
}


sub run
{
  my ($cmd) = @_;
  note "Running: ", $cmd;
  system($cmd);
  my $ecode = $? >> 8;
  die "Command '$cmd' exited with code $ecode." if $ecode;
}


sub main
{
  # Verify parameters
  note "        Workdir: ", $OPTS{workdir};
  note "       Username: ", $OPTS{username};
  note "From repository: ", $OPTS{from};
  note "  To repository: ", $OPTS{to};
  mkdir "$OPTS{workdir}" unless -f $OPTS{workdir};
  
  # verify plugin installed
  verify_tools();
  
  # grab the repo from hg, push it to git
  convert($OPTS{from}, $OPTS{to});
}


sub verify_tools
{
  local $@;
  eval {
    run "hg help git >/dev/null";
  };
  if ($@) {
    note "Error:", "it looks like you don't have the hg-git extension installed.", 'red';
    say "See installation instructions at https://www.mercurial-scm.org/wiki/HgGit or check your package manager.";
    exit 1;
  }
}


sub convert
{
  my ($from, $to) = @_;
  local $@;
  eval {
    chdir "$OPTS{workdir}" or die "Couldn't enter workdir.";
    die "Clone directory exists already, refusing to act." if -d $from;
    run "hg clone ssh://hg\@$OPTS{baseurl}/$OPTS{username}/$from";

    chdir "$OPTS{workdir}/$from" or die "Couldn't enter repo dir '$OPTS{workdir}/$from'.";
    run "hg bookmark master";
    run "hg push git+ssh://git\@$OPTS{baseurl}/$OPTS{username}/$to";
    
  };
  if ($@) {
    note "Error:", $@, 'red';
    exit 1;
  }
}


help() if $OPTS{help};
help("Please specify your username with --username") unless $OPTS{username};
help("Please specify repository to pull from with --from") unless $OPTS{from};
$OPTS{to} = $OPTS{from} =~ s/^$OPTS{prefix}//r unless $OPTS{to};
main();


