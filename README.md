# Bitbucket Mercurial Migration Tool

It's going away, so I need something to help me convert these repositories

# Target Audience

 * People using BitBucket for the code storage of their hobby repos, that use Mercurial.
 * Since only the code gets moved over, this is unsuitable for anyone using features like the wiki, issue tracker, PRs, etc.
 * Also it's just using the basic `hg-git` extension, and won't convert hg:branches into git:branches.

# Preparation

 * First, view your repositories and filter for the Mercurial ones: https://bitbucket.org/dashboard/repositories?scm=hg
 * Next, because I'm too lazy to actually script the API for this, go into each project and rename it:
    * go to Settings
    * Under "Update repository details" you can change the name
    * Click "Save repository details"
    * For the purposes of this script, I'm renaming each repo to have a `hg-` prefix. Do `decurse-p5` becomes `hg-decurse-p5`.
 * Then you need to create an empty Git repo with the old name.
 * Make sure you have the `hg-git` extension installed. https://www.mercurial-scm.org/wiki/HgGit

# Usage

It's pretty basic; just automating the cloning and pushing for you.

```
Usage: ./hg2git.pl --username my-name --from $OPTS{prefix}my-repo <--to my-repo>
  Converts the given Mercurial repo to a Git one, and pushes to Bitbucket.
  
  --prefix $OPTS{prefix}
    Change the expected repository name prefix that indicates a Mercurial repo.
  
  --baseurl $OPTS{baseurl}
    Change url of bitbucket.org. In case you're using ssh config hosts to switch keys.
  
  --workdir $OPTS{workdir}
    Change where we download repositories to.
  
  --username <username>
    Specify what your bitbucket username is, for building paths.
  
  --from <hg-repo>
    Specify the name of the hg repo to convert.
  
  --to <git-repo>
    Specify the name of the git repo to push to.
    If not given, will be inferred based on the hg repo name.
```

![Example](/bitbucket-hg-migration.png)

# Caveats

So many.

Aside from the 'only really works with your master branch' thing, you're also having to set up the project in Bitbucket again yourself, so there's the public/private toggle to get right and a description or icon to fix up if you're feeling really dilligent. Also it's not gonna convert your `.hgignore` file to a `.gitignore`.

## Code didn't show up in Bitbucket

After doing the push to the new git repo, always check that it's showing what you expect in Bitbucket. In my case, the `dice-roller` repo was mysteriously blank, despite definitely having checked out the `hg` equivalent and definitely pushing objects up.

When doing the git clone, I got the message: `remote HEAD refers to nonexistent ref, unable to checkout.`. This appears to be because `dice-roller` was the one hg repo I was doing anything more than the most basic of tasks: I had tagged particular releases. It looks like the hg-git extension then pushed those tagged releases as branches in git, and didn't set up a `master` branch pointer for git to pull from.

```
james@yin(): ~/source/bitbucket
$ git clone ssh://git@bitbucket.org/jamesneko/dice-roller
Cloning into 'dice-roller'...
Enter passphrase for key '/home/james/.ssh/bitbucket_rsa': 
remote: Counting objects: 408, done.
remote: Compressing objects: 100% (306/306), done.
remote: Total 408 (delta 0), reused 408 (delta 0)
Receiving objects: 100% (408/408), 175.19 KiB | 147.00 KiB/s, done.
warning: remote HEAD refers to nonexistent ref, unable to checkout.


james@yin(): ~/source/bitbucket
$ cd dice-roller/

james@yin(): ~/source/bitbucket/dice-roller (master)
$ git ls-remote origin
Enter passphrase for key '/home/james/.ssh/bitbucket_rsa': 
7228aa1891b0da157ce29e708ef7ff0251f257af        refs/tags/0.1.0
de2d721c74ef69f482a1de9d720a879b18588024        refs/tags/0.1.1
7ace97501871d122058d4f767f8f65faf77491c2        refs/tags/0.1.2

james@yin(): ~/source/bitbucket/dice-roller (master)
$ git branch -a

james@yin(): ~/source/bitbucket/dice-roller (master)
$ git checkout master
error: pathspec 'master' did not match any file(s) known to git.
```

Quick look at the old hg repo confirms that `refs/tags/0.1.2` should be the latest that I want, at least:

```
changeset:   84:13a53d772d50
tag:         tip
user:        James Clark <james@lazycat.com.au>
date:        Thu Mar 10 23:03:40 2016 +1100
summary:     Added tag 0.1.2 for changeset ff8b7e99a2cc

changeset:   83:ff8b7e99a2cc
tag:         0.1.2
user:        James Clark <james@lazycat.com.au>
date:        Wed Mar 09 01:31:51 2016 +1100
summary:     Exceptions.

changeset:   82:b56868a8be46
user:        James Clark <james@lazycat.com.au>
date:        Tue Mar 08 15:21:05 2016 +1100
summary:     README improvements.
```

To fix, I just checked out the latest release and called it `master`.

```
james@yin(): ~/source/bitbucket/dice-roller (master)
$ git checkout refs/tags/0.1.2
Note: checking out 'refs/tags/0.1.2'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -b with the checkout command again. Example:

  git checkout -b <new-branch-name>

HEAD is now at 7ace975... Exceptions.

james@yin(): ~/source/bitbucket/dice-roller ((0.1.2))
$ git checkout -b master
Switched to a new branch 'master'

james@yin(): ~/source/bitbucket/dice-roller (master)
$ git push origin master
Enter passphrase for key '/home/james/.ssh/bitbucket_rsa': 
Total 0 (delta 0), reused 0 (delta 0)
To ssh://bitbucket.org/jamesneko/dice-roller
 * [new branch]      master -> master
```

In another case, it seems I had one old release to work with but nothing accounting for a few tweaks added later. I've since modified the script to include a `hg bookmark master` command, which will equate to a similarly named branch on the final git repo.

